Projects
========

.. list-table::

   * - .. image:: c2dm.png
          :width: 189px
          :height: 124px
          :target: c2dm/c2dm.html
     - | :ref:`c2dm`
       | Structural and electronic properties calculated with different DFT XC functionals and G0W0 of a large range of 2D materials. At the moment the database contains 216 transition-metal oxides and chalcocogenides.

   * - .. image:: vdwh.png
          :width: 230px
          :target: vdwh/vdwh.html
     - | :ref:`vdwh`
       | The dielectric building blocks of 51 transition metal dichalcogenides, hexagonal boron nitride, and graphene is available from this database. The results are used to calculate the dielectric function of van der Waals heterostructures, using the Quantum Electrostratic Heterostructure (QEH) model.

   * - .. image:: organometal.png
          :width: 189px
          :height: 124px
          :target: organometal/organometal.html
     - | :ref:`organometal`
       | We have performed electronic structure calculations of 240 perovskites composed of Cs, CH3NH3, and HC(NH2)2 as A-cation, Sn and Pb as B-ion, and a combination of Cl, Br, and I as anions.

   * - .. image:: dssc.png
          :width: 189px
          :height: 124px
          :target: dssc/dssc.html
     - | :ref:`dssc`
       | We present a computational screening study of more than 12,000 porphyrin-based dyes obtained by modifying the porphyrin backbone (metal center and axial ligands), substituting hydrogen by fluorine, and adding different side and anchoring groups.

   * - .. image:: mp_gllbsc.png
          :width: 189px
          :height: 124px
          :target: mp_gllbsc/mp_gllbsc.html
     - | :ref:`mp_gllbsc`
       | Electronic bandgap calculations are presented for 2400 experimentally known materials from the Materials Project database and the bandgaps, obtained with different types of functionals within density functional theory and (partial) self-consistent GW approximation, are compared for 20 randomly chosen compounds forming an unconventional set of ternary and quaternary materials.

   * - .. image:: cubic_perovskites.png
          :width: 189px
          :height: 124px
          :target: cubic_perovskites/cubic_perovskites.html
     - | :ref:`cubic_perovskites`
       | We perform computational screening of around 19 000 oxides, oxynitrides, oxysulfides, oxyfluorides, and oxyfluoronitrides in the cubic perovskite structure with photoelectrochemical cell applications in mind.

   * - .. image:: low_symmetry_perovskites.png
          :width: 189px
          :height: 124px
          :target: low_symmetry_perovskites/low_symmetry_perovskites.html
     - | :ref:`low_symmetry_perovskites`
       | We have used density functional theory (DFT) calculations to investigate 300 oxides and oxynitrides in the Ruddlesden–Popper phase of the layered perovskite structure.

   * - .. image:: absorption_perovskites.png
          :width: 189px
          :height: 124px
          :target: absorption_perovskites/absorption_perovskites.html
     - | :ref:`absorption_perovskites`
       | We calculate the optical properties of a set of oxides, oxynitrides, and organometal halide cubic and layered perovskites with a bandgap in the visible part of the solar spectrum.

   * - .. image:: funct_perovskites.png
          :width: 189px
          :height: 124px
          :target: funct_perovskites/funct_perovskites.html
     - | :ref:`funct_perovskites`
       | We investigate the band gaps and optical spectra of functional perovskites composed of layers of the two cubic perovskite semiconductors BaSnO3-BaTaO2N and LaAlO3-LaTiO2N.

   * - .. image:: beef.png
          :width: 189px
          :height: 124px
          :target: beef/beef.html
     - | :ref:`beef`
       | We present a general-purpose meta-generalized gradient approximation (MGGA) exchange-correlation functional generated within the Bayesian error estimation functional framework.

   * - .. image:: catapp.png
          :width: 189px
          :height: 124px
          :target: catapp/catapp.html
     - | :ref:`catapp1`
       | CatApp data: Calculated reaction and activation energies for elementary coupling reactions occurring on metal surfaces

   * - .. image:: dcdft.png
          :width: 189px
          :height: 124px
          :target: dcdft/dcdft.html
     - | :ref:`dcdft`
       | Benchmark: codes precision measured using the database of bulk systems from http://molmod.ugent.be/DeltaCodesDFT.

   * - .. image:: tmfp06d.png
          :width: 189px
          :height: 124px
          :target: tmfp06d/tmfp06d.html
     - | :ref:`tmfp06d`
       | Benchmark: the performance of semilocal and hybrid density functionals in 3d transition-metal chemistry (reproducing published results).

   * - .. image:: fcc111.png
          :width: 189px
          :height: 124px
          :target: fcc111/fcc111.html
     - | :ref:`fcc111`
       | Benchmark: adsorption energy of atomic oxygen and carbon on fcc111.

   * - .. image:: compression.png
          :width: 189px
          :height: 124px
          :target: compression/compression.html
     - | :ref:`compression`
       | Benchmark: compression energies of bulk fcc and rocksalt.

   * - .. image:: gbrv.png
          :width: 189px
          :height: 124px
          :target: gbrv/gbrv.html
     - | :ref:`gbrv`
       | Benchmark: Pseudopotentials for high-throughput DFT calculations (reproducing published results).

   * - .. image:: g2.png
          :width: 189px
          :height: 124px
          :target: g2/g2.html
     - | :ref:`g2`
       | Benchmark: PBE atomization energies and structures of the G2/97 set of molecules.


.. toctree::
    :hidden:

    organometal/organometal
    dssc/dssc
    mp_gllbsc/mp_gllbsc
    cubic_perovskites/cubic_perovskites
    low_symmetry_perovskites/low_symmetry_perovskites
    absorption_perovskites/absorption_perovskites
    funct_perovskites/funct_perovskites
    beef/beef
    dcdft/dcdft
    c2dm/c2dm
    vdwh/vdwh
    tmfp06d/tmfp06d
    fcc111/fcc111
    compression/compression
    gbrv/gbrv
    g2/g2
    catapp/catapp

What is CMR?
------------

.. toctree::

    about


Older CMR-projects
------------------

* `DF for surface science
  <https://cmr1.fysik.dtu.dk/cmr/var/view/BEEF-vdW/index.php>`_
* `One- and Two-photon Water Splitting and Transparent Shielding
  <https://cmr1.fysik.dtu.dk/cmr/var/view/tandem/index.php>`_
* `Reversible H storage
  <https://cmr1.fysik.dtu.dk/cmr/var/view/10.1063_1.3148892/index.php>`_
* `Stability and band gaps
  <https://cmr1.fysik.dtu.dk/cmr/var/view/10.1039_C1EE02717D/index.php>`_


Other links
-----------

* `Browse all data <http://cmrdb.fysik.dtu.dk>`_
* `ASE-DB <https://wiki.fysik.dtu.dk/ase/ase/db/db.html>`_
* :ref:`genindex`
* :ref:`search`


.. toctree::
    :maxdepth: 1

    server
