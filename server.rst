.. _server:
.. highlight:: bash

=======================
Setting up a web-server
=======================

The CMR web-pages
=================

To get started from scratch::
    
  $ cd ~
  $ mkdir sphinx
  $ cd sphinx
  $ git clone https://gitlab.com/camd/cmr.git
  $ git clone https://gitlab.com/ase/ase.git
  $ mkdir downloads
  
Put db-files and images for the front-page in the *downloads* folder. Then run
:download:`build_web_page.py` from your crontab::
    
  MAILTO=...
  PP=$HOME/sphinx/ase:$PYTHONPATH
  */15 * * * * cd ~/sphinx && PYTHONPATH=$PP python cmr/build_web_page.py > cmr.log
  
This will create a new compressed tar-file ``cmr-web-page.tar.gz`` when there
are changes in the ``cmr`` or ``ase`` Git-repositories.  The tar-file contains
static HTML and can be copied toa web-server and unpacked there.
  

The CMR database
================

See https://intra4.fysik.dtu.dk/it/Niflheim_cmrdb-6.
