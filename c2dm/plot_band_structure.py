# creates: H-MoS2_band_structure.png
import ase.db
from ase.db.plot import dct2plot

name = 'MoS2'
phase = 'H'

txtname = phase + '-' + name.replace('2', '$_2$')

# Connect to database
db = ase.db.connect('c2dm.db')
# Get the db row
row = db.get(name=name, phase=phase, xc='LDA')

bs = row.data['bs']
bs['title'] = 'Band structure of ' + txtname
dct2plot(row.data, 'bs',
         '{}-{}_band_structure.png'.format(phase, name), show=False)
